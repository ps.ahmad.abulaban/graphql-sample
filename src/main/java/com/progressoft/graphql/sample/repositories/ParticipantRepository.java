package com.progressoft.graphql.sample.repositories;

import com.progressoft.graphql.sample.entities.Participant;
import com.progressoft.graphql.sample.repositories.jpa.ExtendedQuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

public interface ParticipantRepository extends CrudRepository<Participant, String>, ExtendedQuerydslPredicateExecutor<Participant> {
}
