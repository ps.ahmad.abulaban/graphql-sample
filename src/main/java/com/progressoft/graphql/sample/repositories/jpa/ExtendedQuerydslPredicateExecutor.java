package com.progressoft.graphql.sample.repositories.jpa;

import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;
import java.util.Optional;

public interface ExtendedQuerydslPredicateExecutor<T> extends QuerydslPredicateExecutor<T> {
    Optional<T> findOne(JPQLQuery<T> query);

    Optional<T> findOne(Expression<T> expression,
                        Predicate predicate);

    List<T> findAll(JPQLQuery<T> query);

    Page<T> findAll(JPQLQuery<T> query, Pageable pageable);

    List<T> findAll(Expression<T> expression,
                    Predicate predicate);

    Page<T> findAll(Expression<T> expression,
                    Predicate predicate,
                    Pageable pageable);
}
