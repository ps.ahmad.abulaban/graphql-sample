package com.progressoft.graphql.sample.repositories;

import com.progressoft.graphql.sample.entities.Position;
import com.progressoft.graphql.sample.repositories.jpa.ExtendedQuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PositionRepository extends CrudRepository<Position, String>, ExtendedQuerydslPredicateExecutor<Position> {
    Optional<Position> findByParticipantCode(String participantCode);
}
