package com.progressoft.graphql.sample.repositories;

import com.progressoft.graphql.sample.entities.Message;
import com.progressoft.graphql.sample.repositories.jpa.ExtendedQuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

public interface MessageRepository extends CrudRepository<Message, String>, ExtendedQuerydslPredicateExecutor<Message> {
}
