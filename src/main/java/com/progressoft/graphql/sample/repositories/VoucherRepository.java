package com.progressoft.graphql.sample.repositories;

import com.progressoft.graphql.sample.entities.Message;
import com.progressoft.graphql.sample.entities.Voucher;
import com.progressoft.graphql.sample.repositories.jpa.ExtendedQuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VoucherRepository extends CrudRepository<Voucher, String>, ExtendedQuerydslPredicateExecutor<Voucher> {

    List<Voucher> findAllByMessage(Message message);
}
