package com.progressoft.graphql.sample.queries;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.progressoft.graphql.sample.entities.Message;
import com.progressoft.graphql.sample.entities.QMessage;
import com.progressoft.graphql.sample.repositories.MessageRepository;
import com.progressoft.graphql.sample.model.MessagePageable;
import com.progressoft.graphql.sample.model.PageInfo;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Projections;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MessageQuery implements GraphQLQueryResolver {

    @Autowired
    private MessageRepository repository;
    private final QMessage q = QMessage.query;

    public MessagePageable getMessages(int page, int size, DataFetchingEnvironment env) {
        Page<Message> messagePage = repository.findAll(Projections.bean(Message.class, QMessage.getSelectedFields(env.getSelectionSet())),
                new BooleanBuilder(),
                PageRequest.of(page, size));
        return new MessagePageable(messagePage.getContent(),
                new PageInfo(messagePage.getNumber(), messagePage.getSize(), messagePage.getTotalElements(), messagePage.getTotalPages()));
    }

    public Optional<Message> getMessage(String id, DataFetchingEnvironment env) {
        return this.repository.findOne(Projections.bean(Message.class, QMessage.getSelectedFields(env.getSelectionSet())),
                new BooleanBuilder().and(q.id.eq(id)));
    }
}
