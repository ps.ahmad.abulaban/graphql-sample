package com.progressoft.graphql.sample.queries;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.progressoft.graphql.sample.entities.Participant;
import com.progressoft.graphql.sample.entities.QParticipant;
import com.progressoft.graphql.sample.model.PageInfo;
import com.progressoft.graphql.sample.model.ParticipantPageable;
import com.progressoft.graphql.sample.repositories.ParticipantRepository;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Projections;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ParticipantQuery implements GraphQLQueryResolver {

    @Autowired
    private ParticipantRepository repository;
    private final QParticipant q = QParticipant.query;

    public ParticipantPageable getParticipants(int page, int size, DataFetchingEnvironment env) {
        Page<Participant> participantPage = this.repository.findAll(Projections.bean(Participant.class, QParticipant.getSelectedFields(env.getSelectionSet())),
                new BooleanBuilder(),
                PageRequest.of(page, size));
        return new ParticipantPageable(participantPage.getContent(),
                new PageInfo(participantPage.getNumber(), participantPage.getSize(), participantPage.getTotalElements(), participantPage.getTotalPages()));
    }

    public Optional<Participant> getParticipant(String code, DataFetchingEnvironment env) {
        return this.repository.findOne(Projections.bean(Participant.class, QParticipant.getSelectedFields(env.getSelectionSet())),
                new BooleanBuilder().and(q.code.eq(code)));
    }
}
