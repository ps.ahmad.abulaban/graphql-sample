package com.progressoft.graphql.sample.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@Entity
public class Participant implements Serializable {

    @Id
    @Column
    private String code;

    @Column
    private String bic;

    @Column
    private String name;

    @Column
    private String shortName;

    @Column
    private String participantType;

    @Column
    private String settlementBank;

    @Column(length = 1024 * 1024, nullable = false)
    private byte[] logo;

}
