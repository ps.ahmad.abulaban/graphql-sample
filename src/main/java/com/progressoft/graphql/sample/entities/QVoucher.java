package com.progressoft.graphql.sample.entities;

import com.querydsl.core.types.Expression;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import graphql.schema.DataFetchingFieldSelectionSet;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class QVoucher extends EntityPathBase<Voucher> {

    public static final QVoucher query = new QVoucher();

    public final StringPath id = createString("id");
    public final DateTimePath<Instant> timestamp = createDateTime("timestamp", Instant.class);
    public final StringPath party = createString("party");
    public final StringPath counterParty = createString("counterParty");
    public final NumberPath<BigDecimal> amount = createNumber("amount", BigDecimal.class);
    public final StringPath type = createString("type");
    public final QMessage message = new QMessage(forProperty("message"));

    private QVoucher() {
        super(Voucher.class, "query");
    }

    public static Expression<?>[] getSelectedFields(DataFetchingFieldSelectionSet selectedFields) {
        List<Expression<?>> expressionList = new ArrayList<>();
        expressionList.add(query.id);
        if (selectedFields.contains("timestamp"))
            expressionList.add(query.timestamp);
        if (selectedFields.contains("party"))
            expressionList.add(query.party);
        if (selectedFields.contains("counterParty"))
            expressionList.add(query.counterParty);
        if (selectedFields.contains("amount"))
            expressionList.add(query.amount);
        if (selectedFields.contains("type"))
            expressionList.add(query.type);
        return expressionList.toArray(new Expression<?>[0]);
    }
}