package com.progressoft.graphql.sample.entities;

import lombok.Data;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@Data
@Entity
public class Message {

    @Id
    private String id;

    @Column
    private Instant timestamp;

    @Column
    private String sourceBankCode;

    @Column
    private String destinationBankCode;

    @Column
    private BigDecimal transactionAmount;

    @Column
    private BigDecimal fees;

    @Column
    private BigDecimal vat;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "message_id")
    private List<Voucher> vouchers;
}
