package com.progressoft.graphql.sample.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

@Data
@Entity
public class Position {

    @Id
    private String id;

    @Column
    private String participantCode;

    @Column
    private BigDecimal netPosition;

    @Column
    private String creditDebitIndicator;

}
