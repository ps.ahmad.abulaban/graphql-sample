package com.progressoft.graphql.sample.entities;

import com.querydsl.core.types.Expression;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import graphql.schema.DataFetchingFieldSelectionSet;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class QMessage extends EntityPathBase<Message> {

    public static final QMessage query = new QMessage();

    public final StringPath id = createString("id");
    public final DateTimePath<Instant> timestamp = createDateTime("timestamp", Instant.class);
    public final StringPath sourceBankCode = createString("sourceBankCode");
    public final StringPath destinationBankCode = createString("destinationBankCode");
    public final NumberPath<BigDecimal> transactionAmount = createNumber("transactionAmount", BigDecimal.class);
    public final NumberPath<BigDecimal> fees = createNumber("fees", BigDecimal.class);
    public final NumberPath<BigDecimal> vat = createNumber("vat", BigDecimal.class);

    private QMessage() {
        super(Message.class, "query");
    }

    QMessage(PathMetadata metadata) {
        super(Message.class, metadata);
    }

    public static Expression<?>[] getSelectedFields(DataFetchingFieldSelectionSet selectedFields) {
        List<Expression<?>> expressionList = new ArrayList<>();
        expressionList.add(query.id);
        if (isFieldSelected(selectedFields, "timestamp"))
            expressionList.add(query.timestamp);
        if (isFieldSelected(selectedFields, "sourceBankCode"))
            expressionList.add(query.sourceBankCode);
        if (isFieldSelected(selectedFields, "destinationBankCode"))
            expressionList.add(query.destinationBankCode);
        if (isFieldSelected(selectedFields, "transactionAmount"))
            expressionList.add(query.transactionAmount);
        if (isFieldSelected(selectedFields, "fees"))
            expressionList.add(query.fees);
        if (isFieldSelected(selectedFields, "vat"))
            expressionList.add(query.vat);
        return expressionList.toArray(new Expression<?>[0]);
    }

    private static boolean isFieldSelected(DataFetchingFieldSelectionSet selectedFields, String fieldName) {
        return selectedFields.contains(fieldName) || selectedFields.contains("data/" + fieldName);
    }
}