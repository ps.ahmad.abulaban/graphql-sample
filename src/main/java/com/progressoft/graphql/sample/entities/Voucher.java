package com.progressoft.graphql.sample.entities;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;

@Data
@Entity
public class Voucher {

    @Id
    private String id;

    @Column
    private Instant timestamp;

    @Column
    private String party;

    @Column
    private String counterParty;

    @Column
    private BigDecimal amount;

    @Column
    private String type;

    @ManyToOne
    @JoinColumn(name = "message_id")
    private Message message;
}
