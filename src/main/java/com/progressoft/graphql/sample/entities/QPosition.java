package com.progressoft.graphql.sample.entities;

import com.querydsl.core.types.Expression;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import graphql.schema.DataFetchingFieldSelectionSet;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class QPosition extends EntityPathBase<Position> {

    public static final QPosition query = new QPosition();

    public final StringPath id = createString("id");
    public final StringPath participantCode = createString("participantCode");
    public final NumberPath<BigDecimal> netPosition = createNumber("netPosition", BigDecimal.class);
    public final StringPath creditDebitIndicator = createString("creditDebitIndicator");

    private QPosition() {
        super(Position.class, "query");
    }

    public static Expression<?>[] getSelectedFields(DataFetchingFieldSelectionSet selectedFields) {
        List<Expression<?>> expressionList = new ArrayList<>();
        expressionList.add(query.id);
        if (selectedFields.contains("participantCode"))
            expressionList.add(query.participantCode);
        if (selectedFields.contains("netPosition"))
            expressionList.add(query.netPosition);
        if (selectedFields.contains("creditDebitIndicator"))
            expressionList.add(query.creditDebitIndicator);
        return expressionList.toArray(new Expression<?>[0]);
    }
}