package com.progressoft.graphql.sample.entities;

import com.querydsl.core.types.Expression;
import com.querydsl.core.types.dsl.ArrayPath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;
import graphql.schema.DataFetchingFieldSelectionSet;

import java.util.ArrayList;
import java.util.List;

public class QParticipant extends EntityPathBase<Participant> {

    public static final QParticipant query = new QParticipant();

    public final StringPath code = createString("code");
    public final StringPath bic = createString("bic");
    public final StringPath name = createString("name");
    public final StringPath shortName = createString("shortName");
    public final StringPath participantType = createString("participantType");
    public final StringPath settlementBank = createString("settlementBank");
    public final ArrayPath<byte[], Byte> logo = createArray("logo", byte[].class);

    private QParticipant() {
        super(Participant.class, "query");
    }

    public static Expression<?>[] getSelectedFields(DataFetchingFieldSelectionSet selectedFields) {
        List<Expression<?>> expressionList = new ArrayList<>();
        expressionList.add(query.code);
        if (isFieldSelected(selectedFields, "bic"))
            expressionList.add(query.bic);
        if (isFieldSelected(selectedFields, "name"))
            expressionList.add(query.name);
        if (isFieldSelected(selectedFields, "shortName"))
            expressionList.add(query.shortName);
        if (isFieldSelected(selectedFields, "participantType"))
            expressionList.add(query.participantType);
        if (isFieldSelected(selectedFields, "settlementBank"))
            expressionList.add(query.settlementBank);
        if (isFieldSelected(selectedFields, "logo"))
            expressionList.add(query.logo);
        return expressionList.toArray(new Expression<?>[0]);
    }

    private static boolean isFieldSelected(DataFetchingFieldSelectionSet selectedFields, String fieldName) {
        return selectedFields.contains(fieldName) || selectedFields.contains("data/" + fieldName);
    }
}