package com.progressoft.graphql.sample.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.progressoft.graphql.sample.entities.Participant;
import org.springframework.stereotype.Component;

import java.util.Base64;

@Component
public class ParticipantResolver implements GraphQLResolver<Participant> {

    public String logo(Participant p) {
        return new String(Base64.getEncoder().encode(p.getLogo()));
    }
}
