package com.progressoft.graphql.sample.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.progressoft.graphql.sample.entities.Message;
import com.progressoft.graphql.sample.entities.QVoucher;
import com.progressoft.graphql.sample.entities.Voucher;
import com.progressoft.graphql.sample.repositories.VoucherRepository;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Projections;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MessageResolver implements GraphQLResolver<Message> {

    @Autowired
    private VoucherRepository repository;
    private final QVoucher q = QVoucher.query;


    public List<Voucher> vouchers(Message message, DataFetchingEnvironment env) {
        return repository.findAll(Projections.bean(Voucher.class, QVoucher.getSelectedFields(env.getSelectionSet())),
                new BooleanBuilder().and(q.message.id.in(message.getId())));
    }
}
