package com.progressoft.graphql.sample.subscription;

import com.coxautodev.graphql.tools.GraphQLSubscriptionResolver;
import com.progressoft.graphql.sample.entities.Position;
import com.progressoft.graphql.sample.entities.QPosition;
import com.progressoft.graphql.sample.repositories.PositionRepository;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Projections;
import graphql.schema.DataFetchingEnvironment;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.time.Duration;

@Component
public class PositionSubscription implements GraphQLSubscriptionResolver {

    @Autowired
    private PositionRepository repository;
    private final QPosition q = QPosition.query;

    public Publisher<Position> position(String participantCode, DataFetchingEnvironment env) {
        return Flux.interval(Duration.ofSeconds(1))
                .map(num -> repository.findOne(Projections.bean(Position.class, QPosition.getSelectedFields(env.getSelectionSet())),
                        new BooleanBuilder().and(q.participantCode.eq(participantCode))).get());
    }
}
