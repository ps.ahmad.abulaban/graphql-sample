package com.progressoft.graphql.sample.model;

import com.progressoft.graphql.sample.entities.Message;
import lombok.Getter;

import java.util.List;

@Getter
public class MessagePageable extends Pageable<Message> {
    public MessagePageable(List<Message> data, PageInfo pageInfo) {
        super(data, pageInfo);
    }
}
