package com.progressoft.graphql.sample.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class PageInfo {
    private int number;
    private int size;
    private long totalElements;
    private int totalPages;
}
