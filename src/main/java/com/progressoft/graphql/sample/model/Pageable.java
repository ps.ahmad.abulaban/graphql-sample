package com.progressoft.graphql.sample.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public abstract class Pageable<T> {
    private List<T> data;
    private PageInfo pageInfo;
}
