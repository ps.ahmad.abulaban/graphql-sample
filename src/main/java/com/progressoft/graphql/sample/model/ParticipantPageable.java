package com.progressoft.graphql.sample.model;

import com.progressoft.graphql.sample.entities.Participant;
import lombok.Getter;

import java.util.List;

@Getter
public class ParticipantPageable extends Pageable<Participant> {
    public ParticipantPageable(List<Participant> data, PageInfo pageInfo) {
        super(data, pageInfo);
    }
}
